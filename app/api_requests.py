import json
import os
from typing import Union

import aiohttp

from app.pydantic_models import UserModel, ExpenseModel, CategoryModel

EXPENSES_API_TOKEN = os.environ.get("EXPENSES_API_TOKEN")
API_URL = os.environ.get("EXPENSES_API_SERVER")

default_headers = {'content-type': 'application/json', "Authorization": "Token " + EXPENSES_API_TOKEN}


async def api_get_verification_code(user: UserModel) -> dict:
    url = f"{API_URL}bot-auth/"
    json_request = user.dict(include={'access_id'})
    headers = default_headers
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def api_get_token(user: UserModel) -> dict:
    url = f"{API_URL}api-token-auth/"
    json_request = user.dict(include={'access_id', 'verification_code'})
    headers = default_headers
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def api_get_group_id_by_invite_code(invite_code: str) -> int:
    url = f"{API_URL}group-invite/"
    json_request = {'invite_code': invite_code}
    headers = default_headers
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def api_get_group(token: str) -> list:
    url = f"{API_URL}api/group/"
    return await _get_request(token=token, url=url)


async def api_new_user(user: UserModel) -> dict:
    url = f"{API_URL}api/users/"
    json_request = user.dict(include={'access_id', 'name', 'user_group'})
    headers = default_headers
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def api_get_all_categories(token: str) -> list:
    url = f"{API_URL}api/categories/"
    return await _get_request(token=token, url=url)


async def api_get_category(category_id: int, token: str) -> dict:
    url = f"{API_URL}api/categories/{category_id}/"
    return await _get_request(token=token, url=url)


async def api_new_category(category: CategoryModel, token: str) -> dict:
    url = f"{API_URL}api/categories/"
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    json_request = category.dict()
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def api_delete_category(category_id: int, token: str):  # -> bool:
    url = f"{API_URL}api/categories/{category_id}/"
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.delete(url=url) as response:
            return response.ok


async def api_new_expense(expense: ExpenseModel, token: str) -> dict:
    url = f"{API_URL}api/my-expenses/"
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    json_request = expense.dict()
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def api_update_expense(expense: ExpenseModel, token: str) -> dict:
    url = f"{API_URL}api/my-expenses/{expense.id}/"
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    json_request = expense.dict()
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.put(url=url, json=json_request) as response:
            print(f'\nPOST {url=}\n{json_request=}\n{headers=}')
            print(f"{response.status=}")
            text = await response.text()
            # print(f"{type(text)} --- {text=}")
            answer = json.loads(text)
            print(f"{type(answer)} --- {answer=}")
            return answer


async def api_get_single_expense(expense_id: int, token: str) -> dict:
    url = f"{API_URL}api/my-expenses/{expense_id}/"
    return await _get_request(token=token, url=url)


async def api_get_my_expenses(token: str) -> list:
    url = f"{API_URL}api/my-expenses/"
    return await _get_request(token=token, url=url)


async def api_get_all_expenses(token: str) -> list:
    url = f"{API_URL}api/all-expenses/"
    return await _get_request(token=token, url=url)


async def api_delete_expense(expense_id: int, token: str) -> bool:
    url = f"{API_URL}api/my-expenses/{expense_id}/"
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.delete(url=url) as response:
            return response.ok


async def api_search_category(token: str, search_text: str) -> Union[dict, list]:
    url = f"{API_URL}api/categories/search/"
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    json_request = {'search': search_text}
    return await _post_request(url=url, json_request=json_request, headers=headers)


async def _get_request(token: str, url: str) -> Union[dict, list]:  # -> tuple[bool, Union[dict, list]]:
    print(f'\nGET {url=}')
    headers = {'content-type': 'application/json', "Authorization": "Token " + token}
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.get(url=url) as response:
            print(f"{response.status=}")
            text = await response.text()
            # print(f"{type(text)} --- {text=}")
            answer = json.loads(text)
            print(f"{type(answer)} --- {answer=}")
            return answer


async def _post_request(url: str, json_request: dict, headers: dict) -> Union[str, int, dict, list]:
    print(f'\nPOST {url=}\n{json_request=}\n{headers=}')
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.post(url=url, json=json_request) as response:
            print(f"{response.status=}")
            text = await response.text()
            # print(f"{type(text)} --- {text=}")
            answer = json.loads(text)
            print(f"{type(answer)} --- {answer=}")
            return answer
