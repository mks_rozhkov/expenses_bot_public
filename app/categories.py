from aiogram import types, Dispatcher

from app.api_requests import api_get_all_categories, api_delete_category, api_new_category, api_get_category
from app.pydantic_models import CategoryModel, UserModel
from app.states_models import ExpenseForm
from app.users import active_users


async def get_categories_keyboard(user: UserModel, callback_prefix: str):
    all_categories = await api_get_all_categories(user.token)
    all_categories.sort(key=lambda x: x['name'])
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    buttons = [types.InlineKeyboardButton(text=category['name'],
                                          callback_data=callback_prefix + str(category['id']))
               for category in all_categories]
    buttons.append(types.InlineKeyboardButton(text='Новая категория', callback_data=callback_prefix + "new"))
    keyboard.add(*buttons)
    return keyboard


async def set_state_category_name(call: types.CallbackQuery):
    await ExpenseForm.new_category_name.set()
    await call.message.edit_text("Введи название новой категории:")


async def new_category(category: CategoryModel, message: types.Message, user: UserModel):
    api_response = await api_new_category(category, user.token)
    created_category = CategoryModel.parse_obj(api_response)
    markup = types.ReplyKeyboardRemove()
    await message.reply(f"Новая категория: <b>{created_category.name}</b>\n", reply_markup=markup,
                        parse_mode=types.ParseMode.HTML)
    state = Dispatcher.get_current().current_state()
    await state.finish()
    return created_category


async def view_category(call: types.CallbackQuery):
    action = call.data.split("_")[1]
    user = active_users.get(call.from_user.id)
    all_categories = await api_get_all_categories(user.token)
    selected_category = next(category for category in all_categories if category['id'] == int(action))
    category_data = await api_get_category(token=user.token, category_id=selected_category['id'])
    keyboard = types.InlineKeyboardMarkup()
    buttons = [types.InlineKeyboardButton(text='Удалить', callback_data=f"category_delete_{category_data['id']}")]
    keyboard.add(*buttons)
    expenses_names = ", ".join({expense['name'] for expense in category_data['expenses']})
    await call.message.edit_text(f"Категория <b>{category_data['name']}</b>\n"
                                 f"Существующие названия трат: {expenses_names}", reply_markup=keyboard,
                                 parse_mode=types.ParseMode.HTML)


async def delete_category(call: types.CallbackQuery):
    user = active_users.get(call.from_user.id)
    category_id = int(call.data.split("_")[2])
    result = await api_delete_category(category_id=category_id, token=user.token)
    if result:
        answer = "удалена!"
    else:
        answer = "не удалена, произошла ошибка!"
    index = call.message.text.find('Существующие названия трат:')
    await call.message.edit_text(f"{call.message.text[:index - 1]} {answer}")
