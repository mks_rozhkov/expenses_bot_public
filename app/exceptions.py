class ApiAuthorizationError(Exception):
    def __init__(self, message=None):
        self.message = message

    def __str__(self):
        if self.message:
            return f'Authorization error. {self.message}'
        else:
            return 'Unexpected authorization error...'
