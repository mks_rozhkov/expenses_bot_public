from datetime import datetime

import pydantic
from aiogram import Dispatcher, types

from app.api_requests import api_new_expense, api_delete_expense, api_get_single_expense, api_update_expense, \
    api_get_my_expenses, api_get_category
from app.categories import get_categories_keyboard
from app.pydantic_models import ExpenseModel, UserModel, CategoryModel
from app.states_models import ExpenseForm
from app.users import active_users


async def update_state_expense_text(expense: ExpenseModel):
    await ExpenseForm.name.set()
    state = Dispatcher.get_current().current_state()
    async with state.proxy() as data:
        data['id'] = expense.id
        data['name'] = expense.name
        data['money'] = expense.money
    await ExpenseForm.category_id.set()


async def new_expense(expense: ExpenseModel, user: UserModel, message: types.Message, category: CategoryModel):
    expense.category = category.id
    if expense.id:
        api_response = await api_update_expense(expense, token=user.token)
    else:
        api_response = await api_new_expense(expense, token=user.token)
    try:
        created_expense = ExpenseModel.parse_obj(api_response)
        await message.answer(f"Сохранено:\n"
                             f"категория: <b>{category.name}</b>\n"
                             f"сумма: <b>{created_expense.money}</b>\n"
                             f"текст: <b>{created_expense.name}</b>", parse_mode=types.ParseMode.HTML)
    except pydantic.error_wrappers.ValidationError:
        error_message = '\n'.join(str(value).strip("{}[]'\" ") for value in api_response.values())
        await message.answer(f"{error_message}")


async def get_sorted_expenses(expenses_list: list, message: types.Message):
    expenses_list.sort(key=lambda x: x['created_at'], reverse=True)
    expenses_sender = message_expense()
    for expense in expenses_list[:10]:
        await expenses_sender(expense, message)


def message_expense():
    temp_date = False

    async def message_expense_inner(expense: dict, message: types.Message):
        nonlocal temp_date
        only_date, formatted_datetime = format_datetime(expense['created_at'])
        if only_date != temp_date:
            await message.answer(f"---------------     <b>{only_date}</b>     ---------------",
                                 parse_mode=types.ParseMode.HTML)
            temp_date = only_date
        await message.answer(f"<b>{expense['user']}</b>\n"
                             f"<b>{expense['money']}</b> - {expense['name']}\n"
                             f"{formatted_datetime}",
                             parse_mode=types.ParseMode.HTML)
        temp_date = only_date

    return message_expense_inner


def format_datetime(raw_datetime):
    from_datetime_format = "%Y-%m-%dT%H:%M:%S.%f"
    to_datetime_format = "%Y-%m-%d %H:%M:%S"
    only_date_format = "%Y-%m-%d"
    datetime_object = datetime.strptime(raw_datetime.rstrip('+03:00'), from_datetime_format)
    only_date = datetime_object.strftime(only_date_format)
    formatted_datetime = datetime_object.strftime(to_datetime_format)
    return only_date, formatted_datetime


async def delete_expense(call: types.CallbackQuery):
    user = active_users.get(call.from_user.id)
    expense_id = int(call.data.replace("last_expense_delete_", ""))
    result = await api_delete_expense(expense_id=expense_id, token=user.token)
    if result:
        answer = "\nудалено!"
    else:
        answer = "\nне удалено, произошла ошибка!"
    await call.message.edit_text(call.message.text + answer)


async def change_expense(call: types.CallbackQuery):
    user = active_users.get(call.from_user.id)
    expense_id = int(call.data.replace("last_expense_change_", ""))
    api_response = await api_get_single_expense(expense_id=expense_id, token=user.token)
    changing_expense = ExpenseModel.parse_obj(api_response)
    print(f'\n\n!!! {changing_expense=}\n\n')
    await update_state_expense_text(changing_expense)
    keyboard = await get_categories_keyboard(user=user, callback_prefix='new_expense_category_')
    await call.message.answer(f'В какую категорию сохранить? "{changing_expense.name}"?', reply_markup=keyboard)


async def cancel_last_expense(message: types.Message, user: UserModel):
    expenses = await api_get_my_expenses(token=user.token)
    expenses.sort(key=lambda x: x['created_at'])
    last_expense = ExpenseModel.parse_obj(expenses[-1])
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    buttons = [types.InlineKeyboardButton(text='Удалить запись',
                                          callback_data='last_expense_delete_' + str(last_expense.id)),
               types.InlineKeyboardButton(text='Изменить категорию',
                                          callback_data='last_expense_change_' + str(last_expense.id))]
    keyboard.add(*buttons)
    _, formatted_datetime = format_datetime(last_expense.created_at)
    category_response = await api_get_category(category_id=last_expense.category, token=user.token)
    category = CategoryModel.parse_obj(category_response)
    await message.answer(f'<b>"{last_expense.name}" - {last_expense.money} рублей</b>\n'
                         f'Категория: {category.name}\n'
                         f'{formatted_datetime}',
                         reply_markup=keyboard, parse_mode=types.ParseMode.HTML)
