import re
from typing import Optional

from pydantic import BaseModel


class UserModel(BaseModel):
    id: Optional[int]
    access_id: int
    name: Optional[str]
    verification_code: Optional[int]
    token: Optional[str]
    user_group: Optional[int]


class ExpenseModel(BaseModel):
    id: Optional[int]
    name: Optional[str]
    money: Optional[int]
    category: Optional[int]
    created_at: Optional[str]

    def parse(self, text: str) -> bool:
        parsed_text = re.split(r'[ ,.]', text)
        if len(parsed_text) > 1:
            for i in range(len(parsed_text)):
                if parsed_text[i].isdecimal():
                    self.money = int(parsed_text[i])
                    parsed_text.pop(i)
                    self.name = " ".join(parsed_text).capitalize()
                    return True
        return False


class CategoryModel(BaseModel):
    id: Optional[int]
    name: str
    user_group: Optional[int]
