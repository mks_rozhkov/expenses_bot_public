from aiogram.dispatcher.filters.state import State, StatesGroup


class UserForm(StatesGroup):
    name = State()
    invite_code = State()


class ExpenseForm(StatesGroup):
    id = State()
    name = State()
    money = State()
    category_id = State()
    new_category_name = State()
