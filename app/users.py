from typing import Optional

from aiogram import types
from aiogram.dispatcher import FSMContext

from app.api_requests import api_get_verification_code, api_get_token, api_new_user, api_get_group_id_by_invite_code
from app.exceptions import ApiAuthorizationError
from app.pydantic_models import UserModel
from app.states_models import UserForm

active_users = {}


async def auth_user(message: types.Message, verify=False) -> Optional[UserModel]:
    """Функция получает данные пользователя из API,
    вносит их в виде UserModel в словарь active_users
    и возвращает их"""

    if not active_users.get(message.from_user.id):
        temp_user = UserModel(access_id=message.from_user.id)
        response = await api_get_verification_code(temp_user)
        if response.get('id') and response.get('name') and response.get('verification_code') and response.get(
                'user_group'):
            temp_user.id = response['id']
            temp_user.name = response['name']
            temp_user.verification_code = response['verification_code']
            temp_user.user_group = response['user_group']
            token_response = await api_get_token(temp_user)
            if token_response.get('token'):
                temp_user.token = token_response['token']
            if temp_user.access_id and temp_user.name and temp_user.verification_code and temp_user.token:
                active_users[temp_user.access_id] = temp_user
            else:
                await message.answer("Ошибка авторизации")
                raise ApiAuthorizationError()
    if verify:
        user = active_users.get(message.from_user.id)
        if not user:
            return
        response = await api_get_verification_code(user)
        active_verification_code = response.get('verification_code')
        if active_verification_code:
            active_users[message.from_user.id].verification_code = active_verification_code
        else:
            await message.answer("Ошибка. Данные не были получены")
            raise ApiAuthorizationError(f'\nAuthorization error.\nAPI response: {response}\n')
    return active_users.get(message.from_user.id)


async def set_state_user_name(message: types.Message, state: FSMContext):
    current_state = await state.get_state()
    if current_state is not None:
        await state.finish()
    await UserForm.name.set()
    await message.reply("Как тебя зовут?")


async def set_user_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
    await set_state_user_invite_code(message=message, state=state)


async def set_state_user_invite_code(message: types.Message, state: FSMContext, first_time=True):
    await UserForm.invite_code.set()
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["У меня нет инвайт-кода"]
    keyboard.add(*buttons)
    message_text = "Введи инвайт-код" if first_time is True else "Ошибка. Еще раз введи инвайт-код"
    await message.answer(message_text, reply_markup=keyboard)


async def register_user(message: types.Message, state: FSMContext):
    if message.text == "У меня нет инвайт-кода":
        user_group = None
    else:
        user_group = await api_get_group_id_by_invite_code(invite_code=message.text)
        if not isinstance(user_group, int):
            await set_state_user_invite_code(message=message, state=state, first_time=False)
            return
    async with state.proxy() as data:
        data['user_group'] = message.text
    temp_user = UserModel(access_id=message.from_user.id, name=data['name'], user_group=user_group)
    created_user = await api_new_user(temp_user)
    if created_user.get('name'):
        await message.reply(f"Привет, {created_user.get('name')}!",
                            parse_mode=types.ParseMode.HTML,
                            reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.reply('Произошла ошибка при регистрации',
                            reply_markup=types.ReplyKeyboardRemove())
    await state.finish()
