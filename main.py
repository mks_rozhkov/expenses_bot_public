import asyncio
import os
from typing import List

from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text, Command
from pydantic import parse_obj_as

from app.api_requests import api_get_my_expenses, api_get_all_expenses, api_search_category, api_get_category, \
    api_get_group
from app.categories import get_categories_keyboard, set_state_category_name, view_category, delete_category, \
    new_category
from app.expenses import new_expense, update_state_expense_text, get_sorted_expenses, delete_expense, change_expense, \
    cancel_last_expense
from app.pydantic_models import CategoryModel, ExpenseModel
from app.states_models import UserForm, ExpenseForm
from app.users import active_users, auth_user, register_user, set_state_user_name, set_user_name

TELEGRAM_BOT_TOKEN = os.environ.get("TELEGRAM_BOT_TOKEN")

bot = Bot(token=TELEGRAM_BOT_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


async def set_commands(bot_: Bot):
    commands = [
        types.BotCommand(command="/categories", description="Список категорий расходов"),
        types.BotCommand(command="/my_expenses", description="Мои последние расходы"),
        types.BotCommand(command="/all_expenses", description="Последние расходы моей группы"),
        types.BotCommand(command="/cancel", description="Изменить/удалить последний расход"),
        types.BotCommand(command="/start", description="Получить код для входа в личный кабинет"),
        types.BotCommand(command="/help", description="Помощь"),
    ]
    await bot_.set_my_commands(commands)


async def finish_all_states():
    state = Dispatcher.get_current().current_state()
    await state.finish()


@dp.message_handler(commands='help')
async def cmd_group(message: types.Message):
    await finish_all_states()
    await message.reply(f"Чтобы добавить расход в базу, отправь сообщение\n"
                        f"с коротким названием траты и суммой.\n"
                        f'Например: "<b>Пиво 500</b>"\n'
                        f'или "<b>500 пиво</b>"\n'
                        f'Система принимает только целые числа.\n'
                        f'Писать "рублей" или "руб." не нужно!',
                        parse_mode=types.ParseMode.HTML)


@dp.message_handler(state='*', commands='start')
async def cmd_start(message: types.Message, state: FSMContext):
    await finish_all_states()
    user = await auth_user(message=message, verify=True)
    if user:
        await message.reply(f"Привет, <b>{user.name}</b>!\n"
                            f"Имя пользователя: <b>{user.access_id}</b>\n"
                            f"Код авторизации: <b>{user.verification_code}</b>\n"
                            f"Моя группа: /group\n"
                            f"Помощь: /help",
                            parse_mode=types.ParseMode.HTML)
    else:
        await set_state_user_name(message=message, state=state)


@dp.message_handler(commands='group')
async def cmd_group(message: types.Message):
    await finish_all_states()
    user = await auth_user(message=message)
    response = await api_get_group(user.token)
    group = response[0]
    user_names = ', '.join([user['name'] for user in group['users']])
    await message.reply(f"Название групппы: {group['name']}\n"
                        f"Всего человек в группе: {len(group['users'])}\n"
                        f"<b>{user_names}</b>\n"
                        f"Инвайт-код группы: <b>{group['invite_code']}</b>\n",
                        parse_mode=types.ParseMode.HTML)


@dp.message_handler(state=UserForm.name)
async def user_name(message: types.Message, state: FSMContext):
    await set_user_name(message=message, state=state)


@dp.message_handler(state=UserForm.invite_code)
async def register(message: types.Message, state: FSMContext):
    await register_user(message=message, state=state)


@dp.message_handler(commands='categories')
async def cmd_categories(message: types.Message):
    await finish_all_states()
    user = await auth_user(message=message)
    keyboard = await get_categories_keyboard(user=user, callback_prefix='category_')
    await message.answer(f"Нажми чтобы отредактировать или удалить категорию:",
                         reply_markup=keyboard, parse_mode=types.ParseMode.HTML)


@dp.callback_query_handler(Text(startswith="category_"))
async def category_callback(call: types.CallbackQuery):
    action = call.data.split("_")[1]
    if action == "new":
        await set_state_category_name(call=call)
    elif action == "delete":
        await delete_category(call=call)
    elif action.isdecimal():
        await view_category(call=call)


@dp.message_handler(state=ExpenseForm.new_category_name)
async def new_category_name_state(message: types.Message, state: FSMContext):
    user = await auth_user(message=message)
    async with state.proxy() as data:
        data['new_category_name'] = message.text
        temp_category = CategoryModel(name=data['new_category_name'], user_group=user.user_group)
    created_category = await new_category(user=user, category=temp_category, message=message)
    if created_category and data.get('name') and data.get('money'):
        temp_expense = ExpenseModel(id=data['id'], name=data['name'], money=data['money'])
        await new_expense(expense=temp_expense, user=user, message=message, category=created_category)
        await finish_all_states()


@dp.message_handler(commands=['my_expenses', 'all_expenses'])
async def cmd_expenses(message: types.Message, command: Command.CommandObj):
    await finish_all_states()
    user = await auth_user(message=message)
    expenses = False
    if command.command == 'my_expenses':
        expenses = await api_get_my_expenses(token=user.token)
    elif command.command == 'all_expenses':
        expenses = await api_get_all_expenses(token=user.token)
    if not expenses:
        await message.answer("Нет трат")
    else:
        await get_sorted_expenses(expenses, message)


@dp.callback_query_handler(Text(startswith="new_expense_category_"), state=ExpenseForm.category_id)
async def new_expense_category_callback(call: types.CallbackQuery, state: FSMContext):
    action = call.data.replace("new_expense_category_", "")
    user = active_users.get(call.from_user.id)
    async with state.proxy() as data:
        temp_expense = ExpenseModel(id=data['id'], name=data['name'], money=data['money'])

    if action == "new":
        await set_state_category_name(call=call)

    elif action == 'another':
        keyboard = await get_categories_keyboard(user=user, callback_prefix='new_expense_category_')
        await call.message.answer(f'В какую категорию сохранить? "{temp_expense.name}"?', reply_markup=keyboard)

    elif action.isdecimal():
        api_response = await api_get_category(category_id=int(action), token=user.token)
        category = CategoryModel.parse_obj(api_response)
        await new_expense(expense=temp_expense, user=user, message=call.message, category=category)
        await finish_all_states()
        await call.message.delete()


@dp.message_handler(state='*', commands='cancel')
async def cmd_cancel(message: types.Message, state: FSMContext):
    data = await state.get_state()
    if data:
        await finish_all_states()
        await message.answer(f'Действие отменено')
    else:
        user = await auth_user(message=message)
        await cancel_last_expense(message=message, user=user)


@dp.message_handler(state=ExpenseForm.category_id)
async def category_callback_error(message: types.Message):
    await message.answer("Выбери нужную категорию выше\nИли нажми /cancel")


@dp.callback_query_handler(Text(startswith="last_expense_delete_"))
async def last_expense_delete(call: types.CallbackQuery):
    await delete_expense(call=call)


@dp.callback_query_handler(Text(startswith="last_expense_change"))
async def last_expense_change(call: types.CallbackQuery):
    await call.message.edit_text(call.message.text + '\nИзменить категорию')
    await change_expense(call=call)


@dp.message_handler()
async def add_expense(message: types.Message):
    user = await auth_user(message=message)
    temp_expense = ExpenseModel()
    if temp_expense.parse(message.text):
        await update_state_expense_text(temp_expense)  # state = ExpenseForm.category_id
        result = await api_search_category(search_text=temp_expense.name, token=user.token)
        received_categories = parse_obj_as(List[CategoryModel], result['categories'])

        if len(received_categories) == 1 and result['match']:
            await new_expense(expense=temp_expense, user=user, message=message, category=received_categories[0])
            await finish_all_states()

        elif received_categories:
            keyboard = types.InlineKeyboardMarkup(row_width=2)
            buttons = [types.InlineKeyboardButton(text=category.name,
                                                  callback_data='new_expense_category_' + str(category.id))
                       for category in received_categories]
            buttons.append(types.InlineKeyboardButton(text='Другая категория',
                                                      callback_data='new_expense_category_new'))
            keyboard.add(*buttons)
            await message.answer(f'В какую категорию сохранить? "{temp_expense.name}"?', reply_markup=keyboard)

        else:
            keyboard = await get_categories_keyboard(user=user, callback_prefix='new_expense_category_')
            await message.answer(f'В какую категорию сохранить? "{temp_expense.name}"?', reply_markup=keyboard)

    else:
        markup = types.ReplyKeyboardRemove()
        await message.answer('Неправильный формат сообщения.\nПомощь: /help', reply_markup=markup)


async def main():
    print('STARTED')
    await set_commands(bot)
    await dp.start_polling()


if __name__ == '__main__':
    asyncio.run(main())
